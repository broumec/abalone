import flask_login
import flask_socketio
from flask import Flask

from back.application.database import db_session, init_db
from back.application.models import User

app = Flask(__name__)
io = flask_socketio.SocketIO(app)

login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login_get'

init_db()

#u = User('Roumec', 'Bryan', 'broumec', '1234')
#db_session.add(u)
#db_session.commit()


@app.teardown_appcontext
def shutdown_session(exception=None):
    db_session.remove()


@app.route('/')
def hello_world():
    return 'Hello, World!'


if __name__ == '__main__':
    io.run(app, debug=True)
