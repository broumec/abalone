from sqlalchemy import Table, Column, Integer, String
from sqlalchemy.orm import mapper
from back.application.database import metadata, db_session


class User(object):
    query = db_session.query_property()

    def __init__(self, name=None, firstname=None, username=None, password=None):
        self.name = name
        self.firstname = firstname
        self.username = username
        self.password = password

    def __repr__(self):
        return '<User %r>' % self.name


users = Table('users', metadata,
              Column('id', Integer, primary_key=True),
              Column('name', String(50), nullable=False),
              Column('firstname', String(50), nullable=False),
              Column('username', String(100), unique=True, nullable=False),
              Column('password', String(150), nullable=False)
              )
mapper(User, users)
