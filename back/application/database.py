from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import scoped_session, sessionmaker

db_user = 'abalone'
db_password = 'mysecretpassword'
db_url = 'localhost:5432'
db_name = 'abalone'

engine = create_engine(f'postgresql+psycopg2://{db_user}:{db_password}@{db_url}', convert_unicode=True)
metadata = MetaData()
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))


def init_db():
    metadata.create_all(bind=engine)
