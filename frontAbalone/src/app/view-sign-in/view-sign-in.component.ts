import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-view-sign-in',
  templateUrl: './view-sign-in.component.html',
  styleUrls: ['./view-sign-in.component.scss']
})
export class ViewSignInComponent implements OnInit {

  hide = true;
  signInForm = new FormGroup({});
  pwd: string;
  pseudo: string;

  constructor() { }

  ngOnInit(): void {
    this.signInForm.addControl("pseudo", new FormControl(this.pseudo, [Validators.required]));
    this.signInForm.addControl("password", new FormControl(this.pwd, [Validators.required]));
  }

}
