import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-view-sign-up',
  templateUrl: './view-sign-up.component.html',
  styleUrls: ['./view-sign-up.component.scss']
})
export class ViewSignUpComponent implements OnInit {

  hide = true;
  hide2 = true;
  signInForm = new FormGroup({});
  pwd: string;
  pseudo: string;

  constructor() { }

  ngOnInit(): void {
    this.signInForm.addControl("fname", new FormControl(this.pseudo, [Validators.required]));
    this.signInForm.addControl("name", new FormControl(this.pseudo, [Validators.required]));
    this.signInForm.addControl("pseudo", new FormControl(this.pseudo, [Validators.required]));
    this.signInForm.addControl("password", new FormControl(this.pwd, [Validators.required]));
    this.signInForm.addControl("confirmPassword", new FormControl(this.pwd, [Validators.required]));
  }

}
