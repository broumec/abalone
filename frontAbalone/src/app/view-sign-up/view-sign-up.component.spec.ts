import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSignUpComponent } from './view-sign-up.component';

describe('ViewSignUpComponent', () => {
  let component: ViewSignUpComponent;
  let fixture: ComponentFixture<ViewSignUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSignUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSignUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
